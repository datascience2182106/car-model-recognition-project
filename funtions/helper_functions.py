import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import numpy as np
import seaborn as sns
import tensorflow as tf
import cv2
import os



def plot_history(history):
    """ 
        This function will visualize model results and help you see how the model generalizes to unseen data during training
    """
    plt.figure(figsize=(12, 6))
    plt.subplot(1,2,1)
    history_keys = list(history.history.keys())
    loss = history.history[history_keys[0]]
    val_loss = history.history[history_keys[2]]
    epochs = range(len(loss))
    plt.plot(epochs, loss, label="loss")
    plt.plot(epochs, val_loss, label="val_loss")
    plt.xlabel("epochs")

    plt.ylabel("loss")
    plt.legend()
    # plt.figure()
    plt.subplot(1,2,2)
    accuracy = history.history[history_keys[1]]
    val_accuracy = history.history[history_keys[3]]
    plt.plot(epochs, accuracy, label="accuracy")
    plt.plot(epochs, val_accuracy, label="val_accuracy")
    plt.legend()
    plt.xlabel("epochs")
    plt.ylabel("accuracy")



def plot_confusion_matrix(model, test_data, val_encoded_labels, class_names_lookup, title):
    plt.figure(figsize=(24,20))
    print(type(class_names_lookup))

    val_pred = np.argmax(model.predict(test_data), axis=1)

    confusion_matrix_data = confusion_matrix(val_encoded_labels, val_pred, labels=list(class_names_lookup.keys()))

    plt.title(title, fontsize=24, fontweight="bold")

    car_names = test_data.class_names

    sns.heatmap(confusion_matrix_data, annot=True, fmt=".0f", cmap="YlGnBu", xticklabels=car_names, yticklabels=car_names,
                annot_kws={"fontsize": 14, "fontweight": "bold"}
    )

    plt.xticks(rotation=90, fontsize=14, fontweight="bold")
    
    plt.yticks(fontsize=14, fontweight="bold", rotation=0)
    
    plt.show()



def load_and_preprocess_image(file_path, filesize=224):
    img = tf.io.read_file(file_path)
    img = tf.io.decode_image(img)
    img = tf.image.resize(img, [filesize, filesize])
    img = img 
    return img[:,:,:3]


def plot_and_predict(model, file_path, class_names):
    """
        This function will plot the image with its prediction as the title.
    """
    preprocessed_img = load_and_preprocess_image(file_path)

    pred = model.predict(tf.expand_dims(preprocessed_img, axis=0))

    print(tf.argmax(pred[0]))

    idx = tf.argmax(pred[0])

    prob = pred[0][idx] * 100

    print(pred, pred[0], "pred")
    class_name = class_names[idx]

    print(tf.argmax(pred[0]), "idx")
    print(class_name)
    plt.imshow(preprocessed_img/255)
    plt.title(f"Prediction: {class_name}   Probability: {prob:.2f}%")
    plt.axis(False)



def predict_multiple_images(list_of_images, model, class_names, folder_path="test_images"):
    try:
        images = np.array(list_of_images).reshape(4,4)

        fig, ax = plt.subplots(4, 4, figsize=(18, 22)) 

        fig.suptitle("Prediction of Unknown Images", fontsize=20, fontweight='bold', y=0.90)  

        plt.subplots_adjust(wspace=0.4, hspace=0.1)  

        for i in range(4):
            for j in range(4):
                image_name = images[i,j]
                preprecessed_image = load_and_preprocess_image(os.path.join(folder_path, image_name))
                pred = model.predict(tf.expand_dims(preprecessed_image, axis=0))

                idx = tf.argmax(pred[0])
                prob = pred[0][idx] * 100

                class_name = class_names[idx]

                image = cv2.imread(f"{folder_path}/{image_name}")
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
                ax[i, j].imshow(preprecessed_image/255)
                ax[i, j].set_title(f"Prediction: {class_name}  Probability: {prob:.2f}")
                ax[i, j].axis("off")

        plt.show() 
    
    except:
        print("Error occured. \nNote: You have to pass list that consists of 16 images.")
