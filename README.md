# Car Recognition Project

Welcome to the Car Recognition Project! This project focuses on recognizing different types of cars using various deep learning models trained through transfer learning techniques. Both feature extraction and fine-tuning methods have been employed to achieve high accuracy in car classification tasks.

## Overview

The project utilizes state-of-the-art deep learning architectures such as EfficientNet (B0, B1), ResNet (V1, V2), and MobileNet (V1, V2) as base models for transfer learning. By leveraging pre-trained weights and fine-tuning on a custom car dataset, the models are capable of accurately identifying and classifying cars from input images. With accuracy rates consistently exceeding 90%, the models demonstrate robust performance across a variety of car types and conditions.

## Key Features

- **Transfer Learning Techniques**: Both feature extraction and fine-tuning methods are employed to leverage pre-trained models for car recognition tasks.
- **Model Selection**: EfficientNet (B0, B1), ResNet (V1, V2), and MobileNet (V1, V2) architectures are chosen for their balance of accuracy and computational efficiency.
- **High Accuracy**: Almost all trained models achieve accuracy rates exceeding 90%, ensuring reliable performance in car classification tasks.
- **Versatility**: The trained models can be integrated into various applications, including vehicle tracking systems, traffic management solutions, and automotive industry applications.

## Model Performance

The performance of each trained model is summarized below:

| Model                       | Accuracy   |
|-----------------------------|------------|
| EfficientNet B0             | ~96%       |
| EfficientNet B1             | ~95%       |
| ResNet V2                   | ~88%       |
| MobileNet V1                | ~93%       |
| Fine-tuned EfficientNet B0  | ~99%       |
| Fine-tuned EfficientNet B1  | ~98%.      |



## Example Images

![Car Recognition Example](result_model.png)

Above is an example image showcasing the capabilities of the trained models in accurately recognizing different types of cars.


## Conclusion

The Car Recognition Project demonstrates the effectiveness of transfer learning techniques in training deep learning models for car classification tasks. By leveraging pre-trained architectures and fine-tuning on custom datasets, the models achieve high accuracy rates and exhibit versatility across various car recognition applications.

